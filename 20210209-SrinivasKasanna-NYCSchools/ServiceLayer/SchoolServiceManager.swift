//
//  SchoolServiceManager.swift
//  20210209-SrinivasKasanna-NYCSchools
//
//  Created by Srinivas Kasanna on 2/9/21.
//

import Foundation

public class SchoolServiceManager {
    //MARK: Singleton
    public static let shared = SchoolServiceManager()
    
    // added as seprate property for easy reusability
    typealias CompletionHandler = (_ success: Bool, [School]?) -> Void
    
    // To fecth schools data
    func fetchSchoolsList(_ completion: @escaping CompletionHandler) {
        guard let url = URL(string: APIEndPoint.schoolList.rawValue) else {
            completion(false, nil)
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard (response as? HTTPURLResponse)?.statusCode == 200, let data = data else {
                completion(false, nil)
                return
            }
            do {
                let schools = try JSONDecoder().decode([School].self, from: data)
                completion(true, schools)
            } catch {
                print(error.localizedDescription)
                completion(false, nil)
            }
        }.resume()
    }
    
    // To fetch school avergae SAT scores
    func fetchSchoolsSATScores(_ completionHandler: @escaping ([SchoolSATScore]) -> Void) {
        guard let url = URL(string: APIEndPoint.schoolDetails.rawValue) else {
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard (response as? HTTPURLResponse)?.statusCode == 200, let data = data else {
                return
            }
            do {
                let scores = try JSONDecoder().decode([SchoolSATScore].self, from: data)
                completionHandler(scores)
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
}

