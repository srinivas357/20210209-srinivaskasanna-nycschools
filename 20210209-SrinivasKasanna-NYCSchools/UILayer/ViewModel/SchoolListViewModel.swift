//
//  SchoolViewModel.swift
//  20210209-SrinivasKasanna-NYCSchools
//
//  Created by Srinivas Kasanna on 2/9/21.
//

import Foundation

struct SchoolCellViewModel {
    var dbn: String
    var schoolName: String
    var address: String
}

public class SchoolViewModel: SchoolListViewConfigurable {
    
    var delegate: SchoolViewModelDelegate?
    ///creating cache to store the scores for temporary instead of getting every time
    private let schoolDetailsCache  = NSCache<NSString, NSArray>()
    var schoolsArray = [School]()
    var schoolSATScores = [SchoolSATScore]()
    
    init() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            self.fetchSchoolData()
        }
    }
    
    var screenTitle: String? {
        return SchoolConstants.schoolListVCTitle
    }
    
    func rowsPerSection(_ section: Int) -> Int {
        schoolsArray.count
    }
    
    func schoolModelFor(_ row: Int) -> SchoolCellViewModel {
        let schoolList = schoolsArray
        let school = schoolList.item(at: row) ?? School()
        let dbn = school.dbn ?? ""
        let schoolName = school.schoolName ?? SchoolConstants.notAvailable
        
        let schoolPrimaryAddressLine1: String
        
        if let primaryAddressLine1 = school.primaryAddressLine1 {
            schoolPrimaryAddressLine1 = "\(primaryAddressLine1), "
        } else {
            schoolPrimaryAddressLine1 = ""
        }
        
        let cityText: String
        
        if let city = school.city {
            cityText = "\(city), "
        } else {
            cityText = ""
        }
        
        let stateCode: String
        
        if let state = school.stateCode {
            stateCode = "\(state), "
        } else {
            stateCode = ""
        }
        
        let zipText = school.zip ?? ""
        
        let schoolFullAddress = "\(schoolPrimaryAddressLine1)\n\(cityText)\(stateCode)\(zipText)"

        return SchoolCellViewModel(dbn:dbn, schoolName: schoolName, address: schoolFullAddress )
    }

    func fetchSchoolData() {
        self.delegate?.showActivityIndicator()
        SchoolServiceManager.shared.fetchSchoolsList { [weak self] isSuccess, schoolList in
            self?.delegate?.hideActivityIndicator()
            guard let self = self else {
                return
            }
            if isSuccess {
                guard let schoolList = schoolList else {
                    self.schoolsArray.removeAll()
                    self.delegate?.reloadSchoolData()
                    return
                }
                self.schoolsArray = schoolList
                self.delegate?.reloadSchoolData()
                if !self.checkCacheDataExist() {
                    self.fetchSchoolAvgSATScores()
                }

            }
        }
    }
    
    func fetchSchoolAvgSATScores() {
        SchoolServiceManager.shared.fetchSchoolsSATScores { [weak self] scores in
            self?.schoolSATScores = scores
            if let schoolSATScores = self?.schoolSATScores {
                self?.schoolDetailsCache.setObject(schoolSATScores as NSArray, forKey: "SATScores")

            }
        }
    }
    
    func checkCacheDataExist() -> Bool {
        return  self.schoolDetailsCache.object(forKey: "SATScores") as? [SchoolSATScore] != nil ? true : false
    }
    
}
