//
//  SchoolListViewConfigurable.swift
//  20210209-SrinivasKasanna-NYCSchools
//
//  Created by Srinivas Kasanna on 2/10/21.
//

import Foundation

protocol SchoolViewModelDelegate {
    func reloadSchoolData()
    func showActivityIndicator()
    func hideActivityIndicator()
}

protocol SchoolListViewConfigurable: AnyObject {
    var delegate: SchoolViewModelDelegate? { get set }
    var screenTitle: String? { get }
    
    func rowsPerSection(_ section:Int) -> Int
}

// Protocol extension to define default behaviour
extension SchoolListViewConfigurable {
    var screenTitle: String? { return nil }
}
