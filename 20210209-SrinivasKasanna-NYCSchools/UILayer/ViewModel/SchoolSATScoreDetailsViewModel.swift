//
//  SchoolSATScoreDetailsViewModel.swift
//  20210209-SrinivasKasanna-NYCSchools
//
//  Created by Srinivas Kasanna on 2/10/21.
//

import Foundation

// susppose to add it as seprate class but added here to make more readble
protocol DetailViewControllerConfigurable: AnyObject {
    var screenTitle: String? { get }
    var schoolSATScore: SchoolSATScore? {get set}
}

class SchoolSATScoreDetailsViewModel: DetailViewControllerConfigurable {
    var screenTitle: String? {
        return SchoolConstants.scoresVCTitle
    }
    var schoolSATScore: SchoolSATScore?
}
