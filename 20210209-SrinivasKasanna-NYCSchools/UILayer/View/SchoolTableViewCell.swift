//
//  SchoolTableViewCell.swift
//  20210209-SrinivasKasanna-NYCSchools
//
//  Created by Srinivas Kasanna on 2/10/21.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with viewModel:SchoolCellViewModel) {
        self.schoolName.text = viewModel.schoolName
        self.schoolAddress.text = viewModel.address
    }
    
    ///Return the class name in String format
    class func reuseIdentifier() -> String {
        return String(describing: self)
    }

}
