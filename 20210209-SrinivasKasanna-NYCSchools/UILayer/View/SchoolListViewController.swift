//
//  SchoolListViewController.swift
//  20210209-SrinivasKasanna-NYCSchools
//
//  Created by Srinivas Kasanna on 2/9/21.
//

import UIKit

/// To show NYC schools information
class NYCSchoolsViewController: UIViewController {

    @IBOutlet weak var schoolsTableView: UITableView!
    var viewModel: SchoolViewModel! {
        didSet {
            viewModel.delegate = self
        }
    }
    private let activityView = UIActivityIndicatorView(style: .large)
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewModel = SchoolViewModel()
        setupNavigationBar()
        setupTableview()
        setupRefreshControl()
    }
    
    //MARK: private helper functions
    fileprivate func setupNavigationBar() {
        navigationItem.title = viewModel.screenTitle
    }

    fileprivate func setupTableview() {
        schoolsTableView.dataSource = self
        schoolsTableView.delegate = self
        //register tableview cell
        schoolsTableView.register(UINib(nibName: SchoolTableViewCell.reuseIdentifier(), bundle: nil), forCellReuseIdentifier: SchoolTableViewCell.reuseIdentifier())

    }
    
    fileprivate func setupRefreshControl() {
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            schoolsTableView.refreshControl = refreshControl
        } else {
            schoolsTableView.addSubview(refreshControl)
        }
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshSchoolList(_:)), for: .valueChanged)
        refreshControl.tintColor = .red
        refreshControl.attributedTitle = NSAttributedString(string: SchoolConstants.pullToRefreshText, attributes: nil)
    }
    
    @objc private func refreshSchoolList(_ sender: Any) {
        // Fetch School data
        viewModel.fetchSchoolData()
    }

    
    // to show alert for no SAT scores
    fileprivate func showNoScoresAlert() {
        let alertView = UIAlertController(title: SchoolConstants.alertTitle, message: SchoolConstants.alertMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: SchoolConstants.alertButtonTitle, style: .default, handler: nil)
        alertView.addAction(okAction)
        self.present(alertView, animated: true, completion: nil)
    }
    
}

//MARK: tableview data source methods
extension NYCSchoolsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rowsPerSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let schoolCell = tableView.dequeueReusableCell(withIdentifier: SchoolTableViewCell.reuseIdentifier(), for: indexPath) as? SchoolTableViewCell else {
            return UITableViewCell()
        }
        schoolCell.backgroundColor = (indexPath.row % 2 == 0) ? UIColor.yellow.withAlphaComponent(0.09) : UIColor.white

        let school = viewModel.schoolModelFor(indexPath.row)
        schoolCell.configure(with: school)
        return schoolCell
    }
}

//MARK: tableview delegate methods
extension NYCSchoolsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let detailsVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "SchoolDetailsViewController") as? SchoolDetailsViewController else {
            return
        }
        let schoolIndex = viewModel.schoolSATScores.firstIndex { SATScore in
            SATScore.dbn == viewModel.schoolModelFor(indexPath.row).dbn

        }
        guard let selectedSchoolIndex = schoolIndex else {
            showNoScoresAlert()
            return
        }
        detailsVC.viewModel = SchoolSATScoreDetailsViewModel()
        detailsVC.viewModel?.schoolSATScore = viewModel.schoolSATScores.item(at: selectedSchoolIndex)
        self.navigationController?.pushViewController(detailsVC, animated: true)

    }
    
}

//MARK: view model delegate methods to update the UI
extension NYCSchoolsViewController: SchoolViewModelDelegate {
    func reloadSchoolData() {
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
            self.schoolsTableView.reloadData()
        }
    }
    
    func showActivityIndicator() {
        activityView.center = self.view.center
        activityView.color = .red
        self.view.addSubview(activityView)
        self.activityView.startAnimating()
    }
    
    func hideActivityIndicator(){
        DispatchQueue.main.async { [weak self] in
            self?.activityView.stopAnimating()
        }
    }

}

