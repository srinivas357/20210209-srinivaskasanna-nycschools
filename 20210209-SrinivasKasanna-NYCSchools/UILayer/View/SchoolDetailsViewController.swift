//
//  SchoolDetailsViewController.swift
//  20210209-SrinivasKasanna-NYCSchools
//
//  Created by Srinivas Kasanna on 2/9/21.
//

import UIKit


/// To show school SAT average scores
class SchoolDetailsViewController: UIViewController {

    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var numberOfSATTestTakers: UILabel!
    @IBOutlet weak var criticalReadingAvgScore: UILabel!
    @IBOutlet weak var mathAvgScore: UILabel!
    @IBOutlet weak var writingAvgScore: UILabel!

    var viewModel: DetailViewControllerConfigurable?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = viewModel?.screenTitle
        // Do any additional setup after loading the view.
        loadScores()
    }
    //MARK: method to load SAT scores
    fileprivate func loadScores() {
        self.schoolName.text = viewModel?.schoolSATScore?.schoolName
        self.numberOfSATTestTakers.text = viewModel?.schoolSATScore?.noOfTestTakers
        self.criticalReadingAvgScore.text = viewModel?.schoolSATScore?.readingScore
        self.mathAvgScore.text = viewModel?.schoolSATScore?.mathScore
        self.writingAvgScore.text = viewModel?.schoolSATScore?.writingScore

    }
}
