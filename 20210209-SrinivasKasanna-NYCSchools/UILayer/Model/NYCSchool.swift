//
//  NYCSchool.swift
//  20210209-SrinivasKasanna-NYCSchools
//
//  Created by Srinivas Kasanna on 2/9/21.
//

import Foundation

//API decodable data - Only picking up important values DBN kind of unique id of school which will be used to navigate to another screen
struct School: Decodable {
    var dbn: String?
    var schoolName: String?
    var overviewParagraph: String?
    var primaryAddressLine1: String?
    var city: String?
    var zip: String?
    var stateCode: String?
    var website: String?
    var phoneNumber: String?
    var grades: String?
    var totalStudents: String?
    
    private enum CodingKeys: String, CodingKey {
        case dbn, city, zip, website
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case primaryAddressLine1 = "primary_address_line_1"
        case stateCode = "state_code"
        case phoneNumber = "phone_number"
        case grades = "finalgrades"
        case totalStudents = "total_students"
    }
}

struct SchoolSATScore: Decodable {
    let dbn: String?
    let schoolName: String?
    let noOfTestTakers: String?
    let readingScore: String?
    let mathScore: String?
    let writingScore: String?

    private enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case noOfTestTakers = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}

