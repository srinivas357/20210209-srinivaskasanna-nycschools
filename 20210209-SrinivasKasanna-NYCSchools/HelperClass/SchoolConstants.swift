//
//  SchoolConstants.swift
//  20210209-SrinivasKasanna-NYCSchools
//
//  Created by Srinivas Kasanna on 2/10/21.
//

import Foundation

enum APIEndPoint: String {
    case schoolList = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    case schoolDetails = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}

struct SchoolConstants {
    static let schoolListVCTitle = "NYC Schools"
    static let scoresVCTitle = "NYC Schools"
    static let alertTitle = "Oops!"
    static let alertMessage = "SAT scores are not avaialble for selected school"
    static let alertButtonTitle = "Dismiss"
    static let notAvailable = "Not Available"
    static let pullToRefreshText = "Fetching Schools Data ..."
}
