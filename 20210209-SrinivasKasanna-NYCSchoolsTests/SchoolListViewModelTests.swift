//
//  SchoolListViewModelTests.swift
//  20210209-SrinivasKasanna-NYCSchoolsTests
//
//  Created by Srinivas Kasanna on 2/11/21.
//

import XCTest
@testable import _0210209_SrinivasKasanna_NYCSchools
// Added limited test cases due to time limits and would prefer to add test cases for 100% code coverage
class SchoolListViewModelTests: XCTestCase {
    var vm: SchoolViewModel!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        vm = SchoolViewModel()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        vm = nil
    }
    
    func testFetchNYCSchoolsData() {
        let expectation = XCTestExpectation(description: "Fetch Schools data")
        SchoolServiceManager.shared.fetchSchoolsList { (success, schools) in
            if success {
                XCTAssert(schools?.count ?? 0 > 0, "schools count is 0")
            } else {
                XCTFail("API failing")
            }
            expectation.fulfill()
        }
       wait(for: [expectation], timeout: 5)
    }
    
    func testFetchSchoolSATScores() {
        let expectation = XCTestExpectation(description: "Fetch Schools SAT Scores")
        SchoolServiceManager.shared.fetchSchoolsSATScores { satScores in
            XCTAssert(satScores.count > 0, "SAT scores not available")
            expectation.fulfill()
        }
       wait(for: [expectation], timeout: 5)

    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
